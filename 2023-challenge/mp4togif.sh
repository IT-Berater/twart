#!/bin/bash

# Script zum Umwandeln von mp4 aus Blender nach git für Web. 
# Aufruf: ./mp4togif.sh DATEINAME.MP4
# Thomas Wenzlaff (c) 2023

filename=$1
echo "Verwende $filename für die Umwandlung von mp4 nach gif"

# erzeugen einer Palette
ffmpeg -i $filename -vf fps=30,scale=512:-1:flags=lanczos,palettegen palette.png

# umwandlung mit der pallete
ffmpeg -i $filename -i palette.png -filter_complex "fps=30,scale=512:-1:flags=lanczos[x];[x][1:v]paletteuse" $filename'.gif'

echo "Umwandlung von $filename nach $filename.gif"
