# Erzeugt ein Mesh in Herzform
# Thomas Wenzlaff

import bpy
import bmesh
import mathutils
import math


# Erzeugen eines leeren Mesh-Objekts und eines leeren BMesh
mesh = bpy.data.meshes.new("HerzMesh")
bm = bmesh.new()

# Herzform definieren
theta = 0
while theta < math.pi * 2:
    r = (2.0 - 2.0 * math.sin(theta) + math.sin(theta) * (abs(math.cos(theta)) ** 0.5) / (math.sin(theta) + 1.4))
    x = r * math.cos(theta)
    y = r * math.sin(theta)
    z = 0
    bm.verts.new((x, y, z))
    theta += 0.01

# Erzeugen von Kanten und Flächen aus den BMesh-Daten
bm.verts.ensure_lookup_table()
bm.edges.ensure_lookup_table()
bm.faces.ensure_lookup_table()
for i in range(1, len(bm.verts)):
    bm.edges.new([bm.verts[i-1], bm.verts[i]])
bm.edges.new([bm.verts[0], bm.verts[-1]])
bm.faces.new(bm.verts)


# Mesh-Objekt mit dem Mesh erstellen und zur Szene hinzufügen
obj = bpy.data.objects.new("HerzObjekt", mesh)
bpy.context.scene.collection.objects.link(obj)

# BMesh freigeben und auf das Mesh-Objekt anwenden
bm.to_mesh(mesh)
mesh.update()