# Erstelle eine image-gallery.html Datei alle im Verzeichnis gespeicherten jpg Dateien
# Thomas Wenzlaff

import os

directory = "/Users/thomaswenzlaff/Movies/Blender/git-repo/twart/Black-Art" # Hier den Pfad zum Verzeichnis angeben

image_files = []

for f in os.scandir(directory):
    if f.name.lower().endswith('.jpg'):
        try:
            image_files.append(f.name)
        except IOError:
            print(f"Die Datei '{f.name}' kann nicht gelesen werden.")

if not image_files:
    print("Es wurden keine JPG-Dateien gefunden.")
else:
    html = "<html><body>"

    for image_file in image_files:
        image_path = os.path.join(directory, image_file)
        image_path_encoded = image_path.replace(" ", "%20")  # encode Leerzeichen
        html += f"<img src='{image_path_encoded}' width='150' height='150'>"
        print(f"Die Datei '{image_path}' wurde erfolgreich eingelesen.")

    html += "</body></html>"

    with open("image_gallery.html", "w") as f:
        f.write(html)
        print("Die HTML-Datei wurde erfolgreich erstellt.")
