# Erstellt in einenm neuen Verzeichnis alle Verzeichnisse aufsteigend
# Thomas Wenzlaff

import os

directory_name = "2024-challenge"
os.mkdir(directory_name)

for i in range(1,52):
    subdirectory_name = os.path.join(directory_name, str(i))
    os.mkdir(subdirectory_name)
