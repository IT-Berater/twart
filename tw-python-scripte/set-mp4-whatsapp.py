import bpy

# Setz das die Werte so, das die Datei per mp4 auch in WhatsApp übertrage werden kann.
#
# set-mp4-whatsapp.py
#
# Thomas Wenzlaff


bpy.context.scene.render.image_settings.file_format = 'FFMPEG'
bpy.context.scene.render.ffmpeg.format = 'MPEG4'
bpy.context.scene.render.ffmpeg.codec = 'H264'
bpy.context.scene.render.ffmpeg.constant_rate_factor = 'MEDIUM'
bpy.context.scene.render.filepath = "//"
