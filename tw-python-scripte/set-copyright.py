import bpy

# Setz das Copyright und schreibt es in das Bild
# Setzt auch das Arbeitsverzeichnis
#
# set-copyright.ph
#
# Thomas Wenzlaff

def setFilePath():
    bpy.context.scene.render.filepath = "//"

def setMetadata():
    bpy.context.scene.render.use_stamp_memory = False
    bpy.context.scene.render.use_stamp_date = False
    bpy.context.scene.render.use_stamp_time = False
    bpy.context.scene.render.use_stamp_render_time = False
    bpy.context.scene.render.use_stamp_frame = False
    bpy.context.scene.render.use_stamp_camera = False
    bpy.context.scene.render.use_stamp_scene = False
    bpy.context.scene.render.use_stamp_filename = False
    bpy.context.scene.render.use_stamp_frame_range = False
    bpy.context.scene.render.use_stamp_hostname = False
    bpy.context.scene.render.use_stamp_lens = False
    bpy.context.scene.render.use_stamp_marker = False
    bpy.context.scene.render.use_stamp_sequencer_strip = False

    bpy.context.scene.render.stamp_note_text = "(c) 2024 kleinhirn.eu"

    bpy.context.scene.render.use_stamp_note = True
    bpy.context.scene.render.use_stamp = True


if __name__ == "__main__":
    setFilePath()
    setMetadata()
    
