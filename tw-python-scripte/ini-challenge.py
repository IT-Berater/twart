import bpy

# Setz das Copyright, Vidoeo und schreibt es in das Bild
# Setzt auch das Arbeitsverzeichnis
#
# set-copyright.ph
#
# Thomas Wenzlaff

def setFilePath(scene):
    scene.render.filepath = "//"

def setMetadata(scene):
    scene.render.use_stamp_memory = False
    scene.render.use_stamp_date = False
    scene.render.use_stamp_time = False
    scene.render.use_stamp_render_time = False
    scene.render.use_stamp_frame = False
    scene.render.use_stamp_camera = False
    scene.render.use_stamp_scene = False
    scene.render.use_stamp_filename = False
    scene.render.use_stamp_frame_range = False
    scene.render.use_stamp_hostname = False
    scene.render.use_stamp_lens = False
    scene.render.use_stamp_marker = False
    scene.render.use_stamp_sequencer_strip = False
    scene.render.use_stamp_note = True
    scene.render.use_stamp = True
    
    scene.render.stamp_note_text = "(c) 2024 kleinhirn.eu"

def setVideodata(scene):
    scene.render.image_settings.file_format = 'FFMPEG'
    scene.render.ffmpeg.format = 'MPEG4'
    scene.render.ffmpeg.codec = 'H264'
    scene.render.ffmpeg.constant_rate_factor = 'MEDIUM'
    scene.frame_end = 90

      
    # Setzen der Framerate auf 30 FPS
    scene.render.fps = 30
  

if __name__ == "__main__":
     # Zugriff auf die aktuelle Szene
    scene = bpy.context.scene
    
    setFilePath(scene)
    setMetadata(scene)
    setVideodata(scene)

    

    

